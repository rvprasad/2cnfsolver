/*
 * Copyright (c) 2019 Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath (rvprasad)
 */

@file:CompilerOpts("-jvm-target 1.8")
@file:DependsOn("com.github.ajalt:clikt:2.8.0")
@file:DependsOn("org.jgrapht:jgrapht-core:1.3.0")

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import org.jgrapht.alg.connectivity.KosarajuStrongConnectivityInspector
import org.jgrapht.graph.DefaultDirectedGraph
import org.jgrapht.graph.DefaultEdge
import java.io.File
import kotlin.math.abs
import kotlin.text.Regex

private typealias Clause = List<Int>
private typealias Assignment = Set<Int>

internal class CommandLine: CliktCommand(name = "kscript solvecnf.kts",
        help = "SCC-based 2-CNF solver",
        printHelpOnEmptyArgs = true) {
    private val cnf by option("-c", "--cnf", help = "2-CNF file in DIMACS format " +
            "(http://www.satcompetition.org/2009/format-benchmarks2009.html)").required()
    override fun run() {
        readClauses(cnf).let { (valid, clauses) ->
            if (valid) {
                SCCBased2CNFSolver.solve(clauses).run {
                    if (isEmpty()) {
                        println("s UNSATISFIABLE")
                    } else {
                        println("s SATISFIABLE")
                        println("v ${joinToString(" ")} 0")
                    }
                }
            }
        }
    }

    private fun readClauses(fileName: String): Pair<Boolean, Collection<Clause>> {
        var expectedNumVars = 0
        var expectedNumClauses = 0
        var valid = true
        val infoRegex = Regex("^p cnf \\d+ \\d+.*")
        val clauseRegex = Regex("^\\s*-?\\d.*")
        val clauses = ArrayList<MutableList<Int>>()
        fun tokenize(line: String) = line.trim().split(' ').filter { it.isNotBlank() }

        File(fileName).bufferedReader().lineSequence().forEachIndexed { idx, line ->
            val tokens = tokenize(line)
            when {
                infoRegex.containsMatchIn(line) -> {
                    expectedNumVars = tokens[2].toInt()
                    expectedNumClauses = tokens[3].toInt()
                }
                clauseRegex.containsMatchIn(line) -> {
                    val lineNum = idx + 1
                    val clause = tokens.map { it.toInt() }.takeWhile { it != 0 }.toMutableList()
                    if (clause.size > 2) {
                        println("c ERR: Clause on line $lineNum has ${clause.size} literals.  Clauses can have " +
                                "at most 2 literals.")
                        valid = false
                    }
                    clauses.add(clause)
                }
            }
        }

        val seenVars = clauses.flatten().map(::abs).distinct()
        println("c Variables ${seenVars.size} seen $expectedNumVars expected")
        println("c Clauses ${clauses.size} seen $expectedNumClauses expected")

        return Pair(valid, clauses)
    }
}

private object SCCBased2CNFSolver {
    private fun simplifyClausesBasedOnValues(clauses: Collection<Clause>, assignment: Assignment) =
            clauses.filterNot { c -> c.any { it in assignment } }.map { c -> c.filterNot { -it in assignment } }

    private sealed class Verdict {
        class Satisfied(val assignment: Assignment): Verdict()
        object Conflicts: Verdict()
        class Unresolved(val clauses: Collection<Clause>, val partialAssignment: Assignment): Verdict()
    }

    private tailrec fun performBCP(clauses: Collection<Clause>, assignment: Assignment = emptySet()): Verdict {
        val valsFromUnitClauses = assignment + clauses.filter { it.size == 1 }.map { it[0] }.toSet()
        val simplifiedClauses = simplifyClausesBasedOnValues(clauses, valsFromUnitClauses)
        val newAssignment = assignment + valsFromUnitClauses
        return when {
            newAssignment.any { -it in newAssignment } || simplifiedClauses.any { it.isEmpty() } -> Verdict.Conflicts
            simplifiedClauses.isEmpty() -> Verdict.Satisfied(newAssignment)
            clauses.size != simplifiedClauses.size -> performBCP(simplifiedClauses, newAssignment)
            else -> Verdict.Unresolved(simplifiedClauses, newAssignment)
        }
    }

    fun solve(givenClauses: Collection<Clause>): Assignment {
        val verdict = performBCP(givenClauses.map { it.distinct() })
        return when (verdict) {
            is Verdict.Conflicts -> emptySet()
            is Verdict.Satisfied -> verdict.assignment
            is Verdict.Unresolved -> {
                val implGraph = DefaultDirectedGraph<Int, DefaultEdge>(DefaultEdge::class.java)
                verdict.clauses.forEach { c ->
                    c.forEach { implGraph.addVertex(it); implGraph.addVertex(-it) }
                    implGraph.addEdge(-c[0], c[1])
                    implGraph.addEdge(-c[1], c[0])
                }

                KosarajuStrongConnectivityInspector(implGraph).stronglyConnectedSets().run {
                    if (any { scc -> scc.any { -it in scc } }) {
                        emptySet()
                    } else {
                        reverse()
                        fold(verdict.partialAssignment) { acc, scc -> acc + scc.filter { -it !in acc && it !in acc } }
                                .sortedBy(::abs).toSet()
                    }
                }
            }
        }
    }
}

CommandLine().main(args)

/*
 * Copyright (c) 2017, Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath (rvprasad)
 */

@Grab(group='org.spockframework', module='spock-core', version='2.0-M3-groovy-3.0', transitive=false)

import spock.lang.Unroll

class TestModelCheck extends spock.lang.Specification {
    def execute2CNFSolverWithArgs(args) {
        final check = (['kscript', 'solve2cnf.kts'] + args).join(' ')
        final outStream = new ByteArrayOutputStream()
        final proc = check.execute()
        proc.consumeProcessOutputStream(new PrintStream(outStream))
        proc.waitFor()
        final sep = System.getProperty('line.separator')
        return outStream.toString().split(sep) as List
    }

    @Unroll
    def 'CNF can have clauses with at most 2 variables'() {
        given: 'CNF with clauses more than 2 clauses'
        final args = ['-c', '../test-inputs/not-2cnf.cnf']

        when: 'solved with solve2cnf.kts'
        final out = execute2CNFSolverWithArgs(args)

        then: '3 output lines are generated'
        out.size() == 3

        and: 'Clause with more than 2 variables is reported.'
        out[0] == 'c ERR: Clause on line 5 has 3 literals.  Clauses can have at most 2 literals.'

        and: 'seen and expected variables are reported'
        out[1] == 'c Variables 3 seen 3 expected'

        and: 'seen and expected clauses are reported'
        out[2] == 'c Clauses 4 seen 4 expected'
    }

    @Unroll
    def '2-CNFs are solved correctly'() {
        when: '2-CNF is solved with solve2cnf.kts'
        final args = ['-c', cnfFile]
        final out = execute2CNFSolverWithArgs(args)

        then: 'expected number of output lines are generated'
        out.size() == size

        and: 'matching seen and expected variables are reported'
        out[0] == "c Variables $vars seen $vars expected"

        and: 'matching seen and expected clauses are reported'
        out[1] == "c Clauses $clauses seen $clauses expected"

        and: 'verdict is correct'
        out[2] == "s $verdict"

        and: 'model is consistent, if satisfiable'
        if (verdict == 'SATISFIABLE') {
            final model = out[3].tokenize()[1..-1]*.toInteger()
            model.every { !(-it in model) }
        }

        and: 'model satisfies the CNF, if satisfiable'
        if (verdict == 'SATISFIABLE') {
            final model = out[3].tokenize()[1..-1]*.toInteger()
            new File(cnfFile).newReader().eachLine { line ->
                if (line ==~ /-?\d.*/) {
                    line = line.trim()
                    final tmp1 = line.tokenize().collect { it.toInteger() }.takeWhile { it != 0 }
                    tmp1.any { it in model }
                }
            }
        }

        where:
        cnfFile                                 || size | vars | clauses | verdict
        '../test-inputs/satisfiable-1.cnf'      || 4    | 7    | 11      | 'SATISFIABLE'
        '../test-inputs/satisfiable-2.cnf'      || 4    | 2    | 3       | 'SATISFIABLE'
        '../test-inputs/satisfiable-3.cnf'      || 4    | 1    | 4       | 'SATISFIABLE'
        '../test-inputs/unsatisfiable-1.cnf'    || 3    | 2    | 4       | 'UNSATISFIABLE'
        '../test-inputs/unsatisfiable-2.cnf'    || 3    | 2    | 3       | 'UNSATISFIABLE'
        '../test-inputs/unsatisfiable-3.cnf'    || 3    | 1    | 4       | 'UNSATISFIABLE'
        '../test-inputs/unsatisfiable-4.cnf'    || 3    | 1    | 2       | 'UNSATISFIABLE'
        '../test-inputs/unsatisfiable-5.cnf'    || 3    | 0    | 1       | 'UNSATISFIABLE'
    }
}
